﻿using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.NUnit3;
using BasketService.Actors;
using BasketService.Models.Baskets;
using BasketService.Models.Baskets.Events;
using BasketService.Models.Baskets.Messages;
using BasketService.Models.Products;
using BasketService.Models.Products.Events;
using BasketService.Models.Products.Messages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasketService.Tests
{
    public class BasketsActorTests : TestKit
    {
        private TestProbe ProductsActor;
        private IActorRef BasketsActor;
        private IDictionary<int, Basket> Baskets;

        [SetUp]
        public void Setup()
        {
            ProductsActor = CreateTestProbe("products");
            Baskets = new Dictionary<int, Basket>();
            Baskets[1] = new Basket()
            {
                Items = new List<BasketItem>() { 
                    new BasketItem()
                    {
                        ProductId = 1,
                        Amount = 2
                    }
                }
            };
            this.BasketsActor = ActorOf(Props.Create<BasketsActor>(Baskets, ProductsActor.Ref));
        }

        [TestCase]
        public void GetBasketByCustomerId_For_Existing_Customer_Should_Return_BasketFound_Event()
        {
            BasketsActor.Tell(new GetBasketByCustomerId(1));
            var result = ExpectMsg<BasketFound>();
            Assert.AreEqual(this.Baskets[1], result.Basket);
        }

        [TestCase]
        public void GetBasketByCustomerId_For_Non_Existing_Customer_Should_Return_BasketNotFound_Event()
        {
            BasketsActor.Tell(new GetBasketByCustomerId(100));
            ExpectMsg<BasketNotFound>();
        }

        [TestCase]
        public void AddProductToBasket_For_Existing_Customer_For_Existing_Product_With_Sufficient_Stock_Should_Return_ProductAdded_Event()
        {
            BasketsActor.Tell(new AddProductToBasket(productId: 0, amount: 1, customerId: 1));
            var product = new Product()
            {
                Id = 0,
                CurrentStock = 10,
                Price = 10.99M,
                Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(0, updateStockMessage.ProductId);
            Assert.AreEqual(1, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new StockUpdated(product));

            var result = ExpectMsg<ProductAdded>();
            Assert.AreEqual(product, result.Product);
            Assert.IsTrue(Baskets[1].Items.Any(b => b.ProductId == 0));
        }

        [TestCase]
        public void AddProductToBasket_For_Existing_Customer_For_Existing_Product_In_Basket_With_Sufficient_Stock_Should_Return_ProductAmountUpdated_Event()
        {
            BasketsActor.Tell(new AddProductToBasket(productId: 1, amount: 1, customerId: 1));
            var product = new Product()
            {
                 Id = 1,
                 CurrentStock = 10,
                 Price = 10.99M,
                 Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(1, updateStockMessage.ProductId);
            Assert.AreEqual(1, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new StockUpdated(product));

            var result = ExpectMsg<ProductAmountUpdated>();
            Assert.AreEqual(Baskets[1].Items.Single(b => b.ProductId == 1).Amount, result.BasketItem.Amount);
        }

        [TestCase]
        public void AddProductToBasket_For_Existing_Customer_For_Non_Existing_Product_Should_Return_ProductNotFound_Event()
        {
            BasketsActor.Tell(new AddProductToBasket(productId: 1, amount: 1, customerId: 1));
            var product = new Product()
            {
                Id = 1,
                CurrentStock = 10,
                Price = 10.99M,
                Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(1, updateStockMessage.ProductId);
            Assert.AreEqual(1, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new Models.Products.Events.ProductNotFound());

            ExpectMsg<Models.Baskets.Events.ProductNotFound>();
        }

        [TestCase]
        public void AddProductToBasket_For_Non_Existing_Customer_Should_Return_ProductAdded_Event()
        {
            BasketsActor.Tell(new AddProductToBasket(productId: 1, amount: 1, customerId: 10));
            var product = new Product()
            {
                Id = 1,
                CurrentStock = 10,
                Price = 10.99M,
                Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(1, updateStockMessage.ProductId);
            Assert.AreEqual(1, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new StockUpdated(product));

            var result = ExpectMsg<ProductAdded>();
            Assert.AreEqual(product, result.Product);
            Assert.IsTrue(Baskets[10].Items.Any(b => b.ProductId == 1));
        }

        [TestCase]
        public void RemoveProductFromBasket_For_Existing_Customer_With_Existing_BasketItem_Should_Return_ProductRemoved_Event()
        {
            BasketsActor.Tell(new RemoveProductFromBasket(productId: 1, amount: 2, customerId: 1));
            var product = new Product()
            {
                Id = 1,
                CurrentStock = 10,
                Price = 10.99M,
                Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(1, updateStockMessage.ProductId);
            Assert.AreEqual(-2, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new StockUpdated(product));

            ExpectMsg<ProductRemoved>();
            Assert.IsTrue(!Baskets[1].Items.Any(b => b.ProductId == 1));
        }

        [TestCase]
        public void RemoveProductFromBasket_For_Existing_Customer_With_Existing_BasketItem_When_Removing_One_Of_Multiple_Should_Return_ProductRemoved_Event()
        {
            BasketsActor.Tell(new RemoveProductFromBasket(productId: 1, amount: 1, customerId: 1));
            var product = new Product()
            {
                Id = 1,
                CurrentStock = 10,
                Price = 10.99M,
                Title = "Test"
            };
            var updateStockMessage = ProductsActor.ExpectMsg<UpdateStock>();
            Assert.AreEqual(1, updateStockMessage.ProductId);
            Assert.AreEqual(-1, updateStockMessage.AmountDecreased);
            ProductsActor.Reply(new StockUpdated(product));

            var result = ExpectMsg<ProductAmountUpdated>();
            Assert.AreEqual(1, result.BasketItem.Amount);
            Assert.AreEqual(1, result.BasketItem.ProductId);
            Assert.IsTrue(Baskets[1].Items.Any(b => b.ProductId == 1));
        }

        //To-Do: Create more unit tests which cover the non-happy path scenario's.
    }
}
