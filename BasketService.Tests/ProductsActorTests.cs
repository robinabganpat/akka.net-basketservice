﻿using Akka.Actor;
using Akka.TestKit.NUnit3;
using BasketService.Actors;
using BasketService.Models.Products;
using BasketService.Models.Products.Events;
using BasketService.Models.Products.Messages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasketService.Tests
{
    public class ProductsActorTests : TestKit
    {
        private IEnumerable<Product> Products;

        private IActorRef ProductsActor;

        [SetUp]
        public void Setup()
        {
            this.Products = new List<Product>()
            {
                new Product()
                {
                    Id = 0,
                    CurrentStock = 10,
                    Price = 650.50M,
                    Title = "OnePlus 6"
                },
                new Product()
                {
                    Id = 1,
                    CurrentStock = 1,
                    Price = 849.99M,
                    Title = "OnePlus 7"
                },
                new Product()
                {
                    Id = 2,
                    CurrentStock = 2,
                    Price = 899.99M,
                    Title = "iPhone 10"
                }
            };
            this.ProductsActor = ActorOf(Props.Create<ProductsActor>(this.Products));
        }

        [TestCase]
        public void GetAllProducts_Should_Return_All_Products()
        {
            ProductsActor.Tell(new GetAllProducts());

            var result = ExpectMsg<IEnumerable<Product>>();
            Assert.AreEqual(this.Products, result);
        }

        [TestCase(0, 1)]
        [TestCase(0, 10)]
        [TestCase(1, 1)]
        [TestCase(0, -10)]
        public void UpdateStock_With_Sufficient_Stock_Should_Return_StockUpdated_Event(int productId, int amount)
        {
            var product = this.Products.Single(p => p.Id == productId);
            int expectedStock = product.CurrentStock - amount;
            ProductsActor.Tell(new UpdateStock(productId, amount));

            var result = ExpectMsg<StockUpdated>();
            Assert.AreEqual(product, result.Product);
            Assert.AreEqual(expectedStock, product.CurrentStock);
            
        }

        [TestCase(0, 11)]
        [TestCase(1, int.MaxValue)]
        public void UpdateStock_With_Sufficient_Stock_Should_Return_InsufficientProductStock_Event(int productId, int amount)
        {
            ProductsActor.Tell(new UpdateStock(productId, amount));
            ExpectMsg<InsufficientProductStock>();
        }

        [TestCase(10, 11)]
        [TestCase(11, 0)]
        public void UpdateStock_For_Non_Existing_Product_Should_Return_ProductNotFound_Event(int productId, int amount)
        {
            ProductsActor.Tell(new UpdateStock(productId, amount));
            ExpectMsg<ProductNotFound>();
        }
    }
}
