﻿using Akka.Actor;
using BasketService.Actors.Providers;
using BasketService.Models.Baskets;
using BasketService.Models.Baskets.Events;
using BasketService.Models.Baskets.Messages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BasketService.Controllers
{
    [ApiController]
    public class BasketsController
    {
        private readonly ILogger<BasketsController> logger;
        private readonly IActorRef basketsActor;

        public BasketsController(BasketsActorProvider basketsActorProvider, ILogger<BasketsController> logger)
        {
            this.logger = logger;
            this.basketsActor = basketsActorProvider.Get();
        }

        /// <summary>
        /// Get the basket of a customer by its customer ID.
        /// </summary>
        /// <param name="customerId">Unique identifier of customer.</param>
        /// <returns></returns>
        [HttpGet("customers/{customerId:int}/basket")]
        public async Task<IActionResult> GetBasketOfCustomer(int customerId)
        {
            logger.LogInformation($"Requesting basket of customer with ID: '{customerId}'.");
            BasketEvent result =  await this.basketsActor.Ask<BasketEvent>(new GetBasketByCustomerId(customerId));
            if(result is BasketFound)
            {
                return new OkObjectResult((result as BasketFound).Basket);
            } else
            {
                return new NotFoundObjectResult($"Cannot find basket for customer with ID: {customerId}");
            }
        }

        /// <summary>
        /// Add an existing product to the specified customer's basket.
        /// </summary>
        /// <param name="customerId">Unique identifier of customer.</param>
        /// <param name="basketItem">Basket item to add to the basket.</param>
        /// <returns></returns>
        [HttpPost("customers/{customerId:int}/basket/products")]
        public async Task<IActionResult> AddProductToBasket(int customerId, [FromBody]BasketItem basketItem)
        {
            logger.LogInformation($"Adding {basketItem.Amount} of product with ID: '{basketItem.ProductId}' to basket for customer with ID: '{customerId}'.");
            var result = await this.basketsActor.Ask<BasketEvent>(new AddProductToBasket(
                basketItem.ProductId,
                basketItem.Amount,
                customerId
            ));

            switch(result)
            {
                case ProductAdded _:
                    return new CreatedResult($"/customers/{customerId}/basket/products", basketItem.ProductId);
                case ProductAmountUpdated updatedItem:
                    return new CreatedResult($"/customers/{customerId}/basket/products", updatedItem.BasketItem.ProductId);
                case ProductNotFound _:
                    return new BadRequestObjectResult($"Product with id: {basketItem.ProductId} is not found.");
                case ProductNotInStock notInStock:
                    return new BadRequestObjectResult($"Product is not in stock for the request amount. Stock left: {notInStock.StockLeft}.");
                case BasketNotFound _:
                    return new NotFoundObjectResult($"Cannot find basket for customer with ID: {customerId}.");
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Remove a product from the basket.
        /// </summary>
        /// <param name="customerId">Unique identifier of customer.</param>
        /// <param name="productId">Unique identifier of product to remove from basket.</param>
        /// <param name="amount">Amount of the specified product to remove from basket. 0 = Remove all products.</param>
        /// <returns></returns>
        [HttpDelete("customers/{customerId:int}/basket/products/{productId:int}/{amount:int?}")]
        public async Task<IActionResult> RemoveProductFromBasket(int customerId, int productId, int amount = 0)
        {
            logger.LogInformation($"Removing {(amount == 0 ? "all" : $"{amount}")} of product '{productId}' from basket of customer with ID: '{customerId}'.");
            var result = await this.basketsActor.Ask<BasketEvent>(new RemoveProductFromBasket(productId, amount, customerId));

            switch (result)
            {
                case ProductRemoved _:
                    return new NoContentResult();
                case ProductAmountUpdated _:
                    return new CreatedResult($"/customers/{customerId}/basket/products", productId);
                case ProductNotFound _:
                    return new BadRequestObjectResult($"Product with id: {productId} is not found.");
                case ProductNotInStock notInStock:
                    return new BadRequestObjectResult($"Product is not in stock for the request amount. Stock left: {notInStock.StockLeft}");
                case BasketNotFound _:
                    return new NotFoundObjectResult($"Cannot find basket for customer with ID: {customerId}");
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
