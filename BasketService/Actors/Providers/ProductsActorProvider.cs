﻿using Akka.Actor;
using BasketService.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Actors.Providers
{
    public class ProductsActorProvider
    {
        private IActorRef ProductsActor { get; }

        public ProductsActorProvider(ActorSystem actorSystem)
        {
            var products = SampleData.Get();
            ProductsActor = actorSystem.ActorOf(Props.Create<ProductsActor>(products), "products");
        }

        public IActorRef Get() => ProductsActor;
    }
}
