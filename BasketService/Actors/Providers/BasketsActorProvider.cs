﻿using Akka.Actor;
using BasketService.Models.Baskets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Actors.Providers
{
    public class BasketsActorProvider
    {
        private IActorRef BasketsActorRef { get; }

        public BasketsActorProvider(ActorSystem actorSystem, ProductsActorProvider productsActorProvider)
        {
            var productsActor = productsActorProvider.Get();
            BasketsActorRef = actorSystem.ActorOf(BasketsActor.Props(new Dictionary<int, Basket>(), productsActor), "Baskets");
        }

        public IActorRef Get() => BasketsActorRef;
    }
}
