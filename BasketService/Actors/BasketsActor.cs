﻿using Akka.Actor;
using BasketService.Models.Baskets;
using BasketService.Models.Baskets.Events;
using BasketService.Models.Baskets.Messages;
using BasketService.Models.Products.Events;
using BasketService.Models.Products.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductNotFound = BasketService.Models.Baskets.Events.ProductNotFound;

namespace BasketService.Actors
{
    public class BasketsActor : ReceiveActor
    {
        private IDictionary<int, Basket> Baskets { get; set; }
        private IActorRef ProductsActorRef { get; set; }
        
        public BasketsActor(IDictionary<int, Basket> baskets, IActorRef productsActor)
        {
            Baskets = baskets;
            ProductsActorRef = productsActor;

            Receive<GetBasketByCustomerId>(e => Sender.Tell(HandleGetBasketByCustomerId(e)));
            ReceiveAsync<AddProductToBasket>(async e => Sender.Tell(await HandleAddProductToBasket(e)));
            ReceiveAsync<RemoveProductFromBasket>(async e => Sender.Tell(await HandleRemoveProductFromBasket(e)));
        }

        public static Props Props(IDictionary<int, Basket> baskets, IActorRef productsActor)
        {
            return Akka.Actor.Props.Create(() => new BasketsActor(baskets, productsActor));
        }

        private async Task<BasketEvent> HandleRemoveProductFromBasket(RemoveProductFromBasket removeProductFromBasket)
        {
            if (Baskets.ContainsKey(removeProductFromBasket.CustomerId))
            {
                var basketItem = Baskets[removeProductFromBasket.CustomerId].Items.FirstOrDefault(b => b.ProductId == removeProductFromBasket.ProductId);
                if (basketItem != null)
                {
                    var productActorResult = await this.ProductsActorRef.Ask<ProductEvent>(new UpdateStock(removeProductFromBasket.ProductId, - removeProductFromBasket.Amount));
                    switch (productActorResult)
                    {
                        case StockUpdated stockUpdated:
                        case Models.Products.Events.ProductNotFound _:
                            if (basketItem.Amount <= removeProductFromBasket.Amount)
                            {
                                Baskets[removeProductFromBasket.CustomerId].Items.Remove(basketItem);
                                return new ProductRemoved();
                            }
                            else
                            {
                                basketItem.Amount -= removeProductFromBasket.Amount;
                                return new ProductAmountUpdated(basketItem);
                            }
                        default:
                            throw new NotImplementedException($"Response of type is not known or expected: {productActorResult.GetType()}.");
                    }
                }
                else
                {
                    return new ProductNotFound();
                }
            }
            else
            {
                return new BasketNotFound();
            }
        }

        private async Task<BasketEvent> HandleAddProductToBasket(AddProductToBasket addProductToBasket)
        {
            if (!Baskets.ContainsKey(addProductToBasket.CustomerId))
            {
                this.Baskets[addProductToBasket.CustomerId] = new Basket();
            }

            if (Baskets.ContainsKey(addProductToBasket.CustomerId))
            {
                var productActorResult = await this.ProductsActorRef.Ask<ProductEvent>(new UpdateStock(addProductToBasket.ProductId, addProductToBasket.Amount));

                switch (productActorResult)
                {
                    case StockUpdated stockUpdated:
                        var existingBasketItem = this.Baskets[addProductToBasket.CustomerId].Items.FirstOrDefault(b => b.ProductId == stockUpdated.Product.Id);
                        if(existingBasketItem == null)
                        {
                            existingBasketItem = new BasketItem()
                            {
                                ProductId = stockUpdated.Product.Id,
                                Amount = addProductToBasket.Amount
                            };
                            this.Baskets[addProductToBasket.CustomerId].Items.Add(existingBasketItem);
                            return new ProductAdded(stockUpdated.Product);
                        }
                        else
                        {
                            existingBasketItem.Amount += addProductToBasket.Amount;
                            return new ProductAmountUpdated(existingBasketItem);
                        }
                    case Models.Products.Events.ProductNotFound _:
                        return new Models.Baskets.Events.ProductNotFound();
                    case InsufficientProductStock insufficientProductStock:
                        return new ProductNotInStock(insufficientProductStock.StockLeft);
                    default:
                        throw new NotImplementedException($"Response of type is not known: {productActorResult.GetType()}.");
                }
            }
            else
            {
                return new BasketNotFound();
            }
        }

        private BasketEvent HandleGetBasketByCustomerId(GetBasketByCustomerId getBasketByCustomerIdEvent)
        {
            if (Baskets.ContainsKey(getBasketByCustomerIdEvent.CustomerId))
            {
                return new BasketFound(Baskets[getBasketByCustomerIdEvent.CustomerId]);
            }
            else
            {
                return new BasketNotFound();
            }
        }
    }
}
