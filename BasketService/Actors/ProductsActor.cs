﻿using Akka.Actor;
using BasketService.Models.Products;
using BasketService.Models.Products.Events;
using BasketService.Models.Products.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Actors
{
    public class ProductsActor : ReceiveActor
    {
        private IList<Product> Products { get; set; }

        public ProductsActor(IList<Product> products)
        {
            this.Products = products;

            Receive<GetAllProducts>(_ => Sender.Tell(new ReadOnlyCollection<Product>(this.Products)));
            Receive<UpdateStock>(p => Sender.Tell(HandleUpdateStock(p)));
        }

        public ProductEvent HandleUpdateStock(UpdateStock updateStock)
        {
            var product = Products.FirstOrDefault(p => p.Id == updateStock.ProductId);

            if(product == null)
            {
                return new ProductNotFound();
            }

            if(product.CurrentStock - updateStock.AmountDecreased >= 0)
            {
                product.CurrentStock -= updateStock.AmountDecreased;
                return new StockUpdated(product);
            }
            else
            {
                return new InsufficientProductStock(product.CurrentStock);
            }
        }
    }
}
