﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Messages
{
    public class RemoveProductFromBasket : BasketMessage
    {
        public int ProductId { get; }
        public int Amount { get; }

        public RemoveProductFromBasket(int productId, int amount, int customerId)
            :base(customerId)
        {
            ProductId = productId;
            Amount = amount;
        }
    }
}
