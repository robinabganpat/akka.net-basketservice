﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Messages
{
    public class GetBasketByCustomerId : BasketMessage
    {
        public GetBasketByCustomerId(int customerId)
            :base(customerId)
        {
        }
    }
}
