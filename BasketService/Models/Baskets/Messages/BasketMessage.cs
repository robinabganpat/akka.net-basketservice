﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Messages
{
    public abstract class BasketMessage
    {
        public int CustomerId { get; }

        public BasketMessage(int customerId)
        {
            CustomerId = customerId;
        }
    }
}
