﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Events
{
    public class ProductNotInStock : BasketEvent
    {
        public int StockLeft { get; }

        public ProductNotInStock(int stockLeft)
        {
            this.StockLeft = stockLeft;
        }
    }
}
