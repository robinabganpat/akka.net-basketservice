﻿using BasketService.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Events
{
    public class ProductAmountUpdated : BasketEvent
    {
        public BasketItem BasketItem { get; }

        public ProductAmountUpdated(BasketItem basketItem)
        {
            this.BasketItem = basketItem;
        }
    }
}
