﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets.Events
{
    public class BasketFound : BasketEvent
    {
        public Basket Basket { get; }

        public BasketFound(Basket basket)
        {
            Basket = basket;
        }
    }
}
