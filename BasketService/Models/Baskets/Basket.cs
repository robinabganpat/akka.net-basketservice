﻿using BasketService.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Baskets
{
    public class Basket
    {
        public IList<BasketItem> Items { get; set; } = new List<BasketItem>();
    }
}
