﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Products.Messages
{
    public class UpdateStock
    {

        public int ProductId { get; }
        public int AmountDecreased { get; }

        public UpdateStock(int productId, int amountDecreased)
        {
            ProductId = productId;
            AmountDecreased = amountDecreased;
        }
    }
}
