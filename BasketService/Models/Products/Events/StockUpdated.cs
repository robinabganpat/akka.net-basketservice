﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Products.Events
{
    public class StockUpdated : ProductEvent
    {
        public Product Product;

        public StockUpdated(Product product)
        {
            Product = product;
        }
    }
}
