﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Models.Products.Events
{
    public class InsufficientProductStock : ProductEvent
    {
        public int StockLeft { get; }

        public InsufficientProductStock(int stockLeft)
        {
            this.StockLeft = stockLeft;
        }
    }
}
