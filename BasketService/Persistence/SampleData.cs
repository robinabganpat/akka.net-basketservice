﻿using BasketService.Models;
using BasketService.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasketService.Persistence
{
    public static class SampleData
    {
        public static IEnumerable<Product> Get() => new List<Product>()
        {
            new Product()
            {
                Id = 0,
                CurrentStock = 10,
                Price = 650.50M,
                Title = "OnePlus 6"
            },
            new Product()
            {
                Id = 1,
                CurrentStock = 10,
                Price = 849.99M,
                Title = "OnePlus 7"
            },
            new Product()
            {
                Id = 2,
                CurrentStock = 10,
                Price = 899.99M,
                Title = "iPhone 10"
            }
        };
    }
}
